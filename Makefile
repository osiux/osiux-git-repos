SHELL:=/bin/bash

update:
	@curl -s https://gitlab.com/osiux/git-bash-utils/-/raw/develop/gitlab2git-repos | bash
	@grep '/osiux/' osiux/.git-repos | sort > .tmp && mv -f .tmp osiux/.git-repos
	@echo "FILTER $$(wc -l osiux/.git-repos)"

list:
	cd osiux && git-repos

clone:
	cd osiux && git-repos -c

pull:
	cd osiux && git-repos -l -a

push:
	cd osiux && git-repos -s -A
