#!/bin/bash

# This script comes with ABSOLUTELY NO WARRANTY, use at own risk
# Copyright (C) 2023 Osiris Alejandro Gomez <osiux@osiux.com>
# Copyright (C) 2023 Osiris Alejandro Gomez <osiris@gcoop.coop>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

function stderr()
{
  echo >&2 "$1"
}

function die()
{
  stderr "$1"
  exit 1
}

[[ -n "$BASH_DEBUG" ]] && set -x
[[ -n "$DIR"        ]] || DIR="$HOME"
[[ -n "$GIT"        ]] || GIT='git'
[[ -n "$OSX"        ]] || OSX="$HOME/$GIT/osiux"
[[ -n "$GBU"        ]] || GBU="$OSX/git-bash-utils"
cd "$DIR"              || die "FAILED TO CHANGE DIRECTORY $DIR"

if [[ -d "$GIT" ]]
then
  stderr "UPDATE osiux-git-repos"
  git pull
else
  stderr "CLONE osiux-git-repos"
  git clone https://gitlab.com/osiux/osiux-git-repos "$GIT" \
    || die "FAILED TO CLONE osiux-git-repos"
fi

if [[ -d "$GBU" ]]
then
  cd "$GBU" || die "FAILED TO CHANGE DIRECTORY $GBU"
  stderr "UPDATE git-bash-utils"
  git pull
else
  stderr "CLONE git-bash-utils"
  git clone https://gitlab.com/osiux/git-bash-utils
  git checkout develop
fi

export PATH="$GBU:$PATH"
cd "$OSX" || die "FAILED TO CHANGE DIRECTORY $OSX"
[[ -e '.git-repos' ]] || die "NOT FOUND $DIR/$GIT/.git-repos"
git-repos
