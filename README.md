# `osiux-git-repos`

Directory index with useful git repositories

## Install

```bash

wget https://gitlab.com/osiux/osiux-git-repos/-/blob/develop/install.sh
/bin/bash ./install.sh

```

## License

_GNU General Public License, GPLv3._

## Author Information

This repo was created in 2023 by
 [Osiris Alejandro Gomez](https://osiux.com/), worker cooperative of
 [gcoop Cooperativa de Software Libre](https://www.gcoop.coop/).
